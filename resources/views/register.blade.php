@extends('layout.master')

@section('judul')
<h1>Form account</h1>
@endsection

@section('content')
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>

    <form action="/welcome" method="post">
    @csrf
    <p>First name:</p>
        <input type="text" name="firstname">

    <p>Last Name:</p>
        <input type="text" name="lastname">

    <p>Gender:</p>
        <input type="radio" name="Gender_user" value="0"> Male <br>
        <input type="radio" name="Gender_user" value="1"> Female <br>
        <input type="radio" name="Gender_user" value="2"> Other

    <p>Nationality:</p>
    <select>
        <option value="0">Indonesia</option>
        <option value="1">Amerika</option>
        <option value="2">Inggris</option>
    </select>

    <p>Language Spoken:</p>
    <input type="checkbox" name="bahasa_user" value="0"> Bahasa Indonesia <br>
    <input type="checkbox" name="bahasa_user" value="1"> English <br>
    <input type="checkbox" name="bahasa_user" value="2"> Other

    <p>Bio:</p>
    <textarea name="biodata" cols="30" rows="10"></textarea> <br><br>

    <input type="submit"value= "Sign Up"> 
    </form>
@endsection