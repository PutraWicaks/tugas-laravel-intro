@extends('layout.master')

@section('judul')
<h1>Index Cast</h1>
@endsection

@section('content')
<a href="/cast/create" class="btn btn-secondary mb-2">Tambah cast</a>
<table class="table table-striped table-dark">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">Action</th>
      </tr>
    </thead>
    <tbody>
    @forelse ($cast as $key => $item)
        <tr>
            <td>{{$key + 1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    <a href="/cast/{{$item->id}}" class="btn btn-primary btn-sm">Show</a>
                    <a href="/cast/{{$item->id}}/edit" class="btn btn-info btn-sm">edit</a>
                    @method('delete')
                    @csrf

                    <input type="submit" class="btn btn-danger btn-sm" value="Delete">
                </form>
            </td>
        </tr>
    @empty
        <tr>
            <td>-</td>
        </tr>
    @endforelse
    </tbody>
  </table>
@endsection